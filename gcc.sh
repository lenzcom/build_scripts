#!/bin/bash

PATH="$HOME/.usr/bin:$HOME/.usr/x86_64-unknown-linux-gnu/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

export PATH

dpkg-buildflags && export CFLAGS CPPFLAGS CXXFLAGS FFLAGS LDFLAGS

build=x86_64-linux-gnu
host=$build
target=x86_64-linux-gnu
linux_arch=x86_64
export build host target linux_arch

gccv=gcc-4.7.1
linuxv=linux-3.2
binutilsv=binutils-2.23.1
mpfr=mpfr-2.4.2
gmp=gmp-4.3.2
mpc=mpc-0.8.1
localsrc=$HOME/.usr/src
export binutilsv gccv linuxv mpfr gmp mpc localsrc

top=$HOME/.usr
src=$top/src
obj=$top/obj
tools=$top/tools
sysroot=$top/sysroot
export top src obj tools sysroot

mkdir -p $src || exit 1
mkdir -p $tools || exit 1
mkdir -p $sysroot || exit 1
chmod -Rc 0755 $src $tools $sysroot

if [ ! -f "$localsrc/$mpfr.tar.bz2"  ]; then
	wget ftp://gcc.gnu.org/pub/gcc/infrastructure/$mpfr.tar.bz2 || exit 1
	tar xjf $mpfr.tar.bz2 -C $src || exit 1
	mv $mpfr.tar.bz2 $localsrc || exit 1
else
	tar xjf "$localsrc/$mpfr.tar.bz2" -C $src || exit 1
fi
ln -sf $src/$mpfr $src/mpfr || exit 1

if [ ! -f "$localsrc/$gmp.tar.bz2"  ]; then
	wget ftp://gcc.gnu.org/pub/gcc/infrastructure/$gmp.tar.bz2 || exit 1
	tar xjf $gmp.tar.bz2 -C $src || exit 1
	mv $gmp.tar.bz2 $localsrc || exit 1
else
	tar xjf "$localsrc/$gmp.tar.bz2" -C $src || exit 1
fi
ln -sf $src/$gmp $src/gmp || exit 1

if [ ! -f "$localsrc/$mpc.tar.gz"  ]; then
	wget ftp://gcc.gnu.org/pub/gcc/infrastructure/$mpc.tar.gz || exit 1
	tar xf $mpc.tar.gz -C $src || exit 1
	mv $mpc.tar.gz $localsrc
else
	tar xf "$localsrc/$mpc.tar.gz" -C $src || exit 1
fi
ln -sf $src/$mpc $src/mpc || exit 1

if [ ! -f "$localsrc/$binutilsv.tar.bz2"  ]; then
	wget ftp://ftp.gnu.org/pub/gnu/binutils/$binutilsv.tar.bz2 || exit 1
	tar xjf $binutilsv.tar.bz2 -C $src || exit 1
	mv $binutilsv.tar.bz2 $localsrc || exit 1
else
	tar xjf "$localsrc/$binutilsv.tar.bz2" -C $src || exit 1
fi
ln -sf $src/$binutilsv $src/binutils || exit 1

if [ ! -f "$localsrc/$linuxv.tar.bz2"  ]; then
	wget ftp://ftp.kernel.org/pub/linux/kernel/v3.x/$linuxv.tar.bz2 || exit 1
	tar xjf $linuxv.tar.bz2 -C $src || exit 1
	mv $linuxv.tar.bz2 $localsrc || exit 1
else
	tar xjf "$localsrc/$linuxv.tar.bz2" -C $src || exit 1
fi
ln -sf $src/$linuxv $src/linux || exit 1

if [ ! -f "$localsrc/$gccv.tar.bz2"  ]; then
	wget ftp://gcc.gnu.org/pub/gcc/releases/$gccv/$gccv.tar.bz2 || exit 1
	tar xjf $gccv.tar.bz2 -C $src || exit 1
	mv $gccv.tar.bz2 $localsrc || exit 1
else
	tar xjf "$localsrc/$gccv.tar.bz2" -C $src || exit 1
fi
ln -sf $src/$gccv $src/gcc || exit 1


pkgs="gmp"

for p in $pkgs
do
	mkdir -p $obj/$p || exit 1
	cd $obj/$p
	env LDFLAGS="-L$tools/lib  -L/usr/lib -L/usr/local/lib ${LDFLAGS}" CPPFLAGS="-I$tools/include -I/usr/include -I/usr/local/include ${CPPFLAGS}" $src/$p/configure \
         --build=$build \
         --prefix=$tools \
         --with-sysroot=$sysroot
	echo "cc1plus fixing 'warnings as errors' error..."
	for f in $(find ./ -writable -readable); do /bin/sed -n -u -i -s 's/\-Werror//g' $f >>/dev/null 2>&1; done
	env PATH=$tools/bin:$PATH make  || exit 1
	env PATH=$tools/bin:$PATH make install  || exit 1
done


pkgs="binutils mpfr mpc"

for p in $pkgs
do
	mkdir -p $obj/$p || exit 1
	cd $obj/$p
	env LDFLAGS="-L$tools/lib  -L/usr/lib -L/usr/local/lib ${LDFLAGS}" CPPFLAGS="-I$tools/include -I/usr/include -I/usr/local/include ${CPPFLAGS}" $src/$p/configure \
         --target=$target \
         --prefix=$tools --with-gmp=$tools \
         --with-sysroot=$sysroot
	echo "cc1plus fixing 'warnings as errors' error..."
	for f in $(find ./ -writable -readable); do /bin/sed -n -u -i -s 's/\-Werror//g' $f >>/dev/null 2>&1; done
	env PATH=$tools/bin:$PATH make  || exit 1
	env PATH=$tools/bin:$PATH make install  || exit 1
done

mkdir -p $obj/gcc   || exit 1
cd $obj/gcc   || exit 1
env LDFLAGS="-L$tools/lib  -L/usr/lib -L/usr/local/lib ${LDFLAGS}" CPPFLAGS="-I$tools/include -I/usr/include -I/usr/local/include ${CPPFLAGS}" $src/$gccv/configure \
       --target=$target \
       --prefix=$tools \
			 --enable-clocale=gnu \
 			 --with-gmp=$tools --with-mpfr=$tools \
 			 --with-mpc=$tools --with-system-zlib  --enable-languages=c   || exit 1
echo "cc1plus fixing 'warnings as errors' error..."
for f in $(find ./ -writable -readable); do /bin/sed -n -u -i -s 's/\-Werror//g' $f >>/dev/null 2>&1; done
env PATH=$tools/bin:$PATH make  || exit 1
env PATH=$tools/bin:$PATH make install  || exit 1

cp -r $src/linux $obj/
cd $obj/linux   || exit 1
PATH=$tools/bin:$PATH \
				make headers_install \
				ARCH=$linux_arch CROSS_COMPILE=$target- \
  			INSTALL_HDR_PATH=$sysroot/usr   || exit 1

mkdir -p $obj/eglibc-headers    || exit 1
cd $obj/eglibc-headers    || exit 1
BUILD_CC=gcc \
    CC=$tools/bin/$target-gcc \
    CXX=$tools/bin/$target-g++ \
    AR=$tools/bin/$target-ar \
    RANLIB=$tools/bin/$target-ranlib \
    $src/libc/configure \
        --prefix=$tools \
        --with-headers=$sysroot/usr/include \
        --build=$build \
        --host=$target \
        --disable-profile --without-gd --without-cvs --enable-add-ons   || exit 1
make install-headers install_root=$sysroot \
        install-bootstrap-headers=yes   || exit 1


# mkdir -p $top/build
#
# cd $top && svn co svn://gcc.gnu.org/svn/gcc/branches/gcc-4_5-branch gcc
#
# CONFFLAGS='"--enable-clocale=gnu" "--with-system-zlib" "--enable-checking=assert" "--with-demangler-in-ld"'
# CONFFLAGS='"$CONFFLAGS "--prefix=/usr/gcc-4.5" "--with-local-prefix=/usr/local" "--enable-gold" "--with-fpmath=sse"'
#
# cd $top/build
# LDFLAGS="-L/usr/lib -L/usr/local/lib" \
#  CPPFLAGS="-I/usr/include -L/usr/local/include" \
#  ../contrib/gcc_build	-o $obj -c $CONFFLAGS configure
#
# ../contrib/gcc_build bootstrap
#

